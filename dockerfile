FROM python:3.9

WORKDIR /src

COPY ./Main.py /src

ENTRYPOINT ["python3", "/src/Main.py"]
